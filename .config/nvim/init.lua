-- Encoding
vim.opt.encoding = 'utf-8'
vim.opt.fileencoding = 'utf-8'

-- Line numbers
vim.opt.number = true
vim.opt.relativenumber = true

-- Indentation
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

-- Do not wrap lines
vim.opt.wrap = false

-- Highlight cursor line
vim.opt.cursorline = false

-- Search options
vim.opt.ignorecase = true
vim.opt.hlsearch = false

-- Key mappings
vim.g.mapleader = ' '

vim.keymap.set('n', '<leader>O', 'O<Esc>o')

vim.keymap.set('n', '<leader>;', 'A;<Esc>')
vim.keymap.set('n', '<leader>{', 'A {}<Esc>i<cr><Esc>O')

vim.keymap.set('n', '<A-j>', 'ddp')
vim.keymap.set('n', '<A-k>', 'ddkkp')

-- Plugins
require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'

    -- Color schemes
    use 'folke/tokyonight.nvim'
end)

-- Color scheme
require('tokyonight').setup({
    style = 'storm',
    transparent = true
})

vim.opt.background = 'dark'
vim.cmd[[colorscheme tokyonight]]

