import subprocess
from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile import hook


GB_COLORS = ["#e92370", "#7ece42", "#d6bb24", "#3b9ebb"]

# Custom widgets
class GameBoyGroupBox(widget.GroupBox):
    def draw(self):
        self.drawer.clear(self.background or self.bar.background)

        offset = self.margin_x
        for i, g in enumerate(self.groups):
            to_highlight = False
            is_block = self.highlight_method == "block"
            is_line = self.highlight_method == "line"

            bw = self.box_width([g])

            if self.group_has_urgent(g) and self.urgent_alert_method == "text":
                text_color = self.urgent_text
            elif g.windows:
                text_color = self.active
            else:
                text_color = self.inactive

            if g.screen:
                if self.highlight_method == "text":
                    border = None
                    text_color = self.this_current_screen_border
                else:
                    if self.block_highlight_text_color:
                        text_color = self.block_highlight_text_color

                    # Custom GameBoy colors
                    border = GB_COLORS[i]
                    to_highlight = True

                    # if self.bar.screen.group.name == g.name:
                    #     if self.qtile.current_screen == self.bar.screen:
                    #         border = self.this_current_screen_border
                    #         to_highlight = True
                    #     else:
                    #         border = self.this_screen_border
                    # else:
                    #     if self.qtile.current_screen == g.screen:
                    #         border = self.other_current_screen_border
                    #     else:
                    #         border = self.other_screen_border
            elif self.group_has_urgent(g) and self.urgent_alert_method in (
                "border",
                "block",
                "line",
            ):
                border = self.urgent_border
                if self.urgent_alert_method == "block":
                    is_block = True
                elif self.urgent_alert_method == "line":
                    is_line = True
            else:
                border = None

            self.drawbox(
                offset,
                g.label,
                border,
                text_color,
                highlight_color=self.highlight_color,
                width=bw,
                rounded=self.rounded,
                block=is_block,
                line=is_line,
                highlighted=to_highlight,
            )
            offset += bw + self.spacing
        self.drawer.draw(offsetx=self.offset, offsety=self.offsety, width=self.width)


class IPAddress(widget.base.InLoopPollText):
    def poll(self):
        ifconfig_output = subprocess.check_output(['ifconfig']).decode('utf-8')

        ip_start = ifconfig_output.find('inet ') + 5
        ip_end = ifconfig_output.find(' ', ip_start)

        return ifconfig_output[ip_start:ip_end]


# Default applications
terminal = 'kitty'
launcher = 'rofi -show run'
browser = 'firefox'

# Mod key = Windows
mod = "mod4"

# Key mappings
keys = [
    Key([mod], "period", lazy.next_screen(), desc = "Change to next monitor"),

    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),

    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "b", lazy.spawn(browser), desc = "Launch browser"),

    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

    Key([mod], "Space", lazy.spawn(launcher), desc = "Span app launcher"),
]

# Groups
group_names = [('WEB', '\U0001f310'), ('DEV', 'dev'), ('TERM', 'term'), ('OTHER', 'others')]
groups = [Group(group_name, label = group_name) for group_name, group_label in group_names]

for i, group in enumerate(groups):
    keys.extend([
        # mod + group position = switch to group
        Key(
            [mod],
            str(i + 1),
            lazy.group[group.name].toscreen(),
            desc="Switch to group {}".format(group.name),
        ),

        # mod + shift + group position = switch to & move focused window to group
        Key(
            [mod, "shift"],
            str(i + 1),
            lazy.window.togroup(group.name, switch_group = True),
            desc="Switch to & move focused window to group {}".format(group.name),
        )
    ])

# Layouts
layouts = [
    layout.MonadTall(
        align = layout.MonadTall._left,
        border_focus = '#9d7cd8',
        border_width = 3,
        single_border_with = 3,
        margin = 8,
        single_margin = 8
    )
]

# Widgets configuration
widget_defaults = dict(
    font = "sans",
    fontsize = 14,
    padding = 8,
)
extension_defaults = widget_defaults.copy()

# Screen configuration
screens = [
    Screen(
        top = bar.Bar(
            [
                widget.Spacer(length = 20),
                GameBoyGroupBox(
                    active = "#8aa7af",
                    inactive = "#8aa7af",
                    block_highlight_text_color = "#ffffff",
                    highlight_method = "line",
                    highlight_color = "#1c1d1f",
                    margin_x = 0
                ),
                widget.Spacer(length = bar.STRETCH),
                widget.Image(filename = '~/.config/qtile/img/ethernet.png', margin_y = 8),
                IPAddress(update_interval = 600),
                widget.Spacer(length = 20),
                widget.Image(filename = '~/.config/qtile/img/clock.png', margin_y = 10),
                widget.Clock(format = "%d/%m/%y  -  %H:%M"),
                widget.Spacer(length = 10),
                widget.Image(filename = '~/.config/qtile/img/power.png', margin_x = 10, margin_y = 8.5)
            ],
            40,
            background = "#273236",
            margin = [8, 8, 0, 8]
        ),
        wallpaper = '~/.config/qtile/img/background.jpg',
        wallpaper_mode = 'fill'
    ),
    Screen(
        wallpaper = '~/.config/qtile/img/background.jpg',
        wallpaper_mode = 'fill'
    )
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

# Autostart script
@hook.subscribe.startup_once
def autostart():
    subprocess.run('/home/srjaimito/.config/qtile/autostart.sh')

